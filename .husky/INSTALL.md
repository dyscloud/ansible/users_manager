# Setup new git repository with pre-commit rules
# Ref: https://github.com/conventional-changelog/commitlint/blob/master/%40commitlint/config-conventional/README.md
npm install --save-dev @commitlint/{cli,config-conventional}
npm install --save-dev husky
npx husky init
echo "npx --no -- commitlint --edit \$1" > .husky/commit-msg
rm -rf .husky/pre-commit
echo "module.exports = {extends: ['@commitlint/config-conventional']};" > commitlint.config.js
echo "node_modules" >> .gitignore
echo "package-lock.json" >> .gitignore
