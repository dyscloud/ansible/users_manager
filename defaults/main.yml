# This file should not be modified.
# You should create a file in host_vars or group_vars to custom the configuration
# Ref: https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html#organizing-host-and-group-variables
---
###############################################################
#       Mandatory parameters to configure                     #
###############################################################

# User database to store following data:
# - Username
# - UID / GID
# - hashed password
# - ssh public key
# e.g:
# users_manager_database:
#   user0:
#     uid:
#       ssh: 1002
#     fullname: "This is a test"
#     password_hash: "$6$ABCDEFGHIJKLMNOPQRSTUVWXYZ" # Can be generate with command "openssl passwd -6"
#     public_key:
#       - key: "ssh-rsa ..."
#         state: present
#       - key: "{{ lookup('file', '/home/user0/.ssh/id_rsa.pub') }}"
#         state: absent

users_manager_database: {}

#############################################

## One of the following parameter must be configured
# In these variables, you'll have to set a list of users from users_manager_database variables.
# Create users on servers

# Must be set in group_vars/all.yml
# users_manager_present_for_all: {}
# e.g:
# users_manager_present_for_all:
#   user0:
#     admin: true # default: false
#     allow_ssh: false # default: true
#     groups:
#       - group_0

# Must be set in a different group than group_vars/all.yml
# users_manager_present_for_group: {}

# Must be set in needed host_vars/<your hosts>.yml's files
# users_manager_present_for_host: {}

#############################################

# Delete manually users on the servers when users_manager_global_configuration.auto_delete_users is set to true
# Must be set in group_vars/all.yml
# users_manager_absent_for_all: []
# e.g:
# users_manager_absent_for_all:
#   - user0

# Must be set in a different group than group_vars/all.yml
# users_manager_absent_for_group: []

# Must be set in needed host_vars/<your hosts>.yml's files
# users_manager_absent_for_host: []

#############################################

# Create groups on servers
# Must be set in group_vars/all.yml
# users_manager_group_present_for_all: []
# e.g:
# users_manager_group_present_for_all:
#   - group_0
#   - group_1

# Must be set in a different group than group_vars/all.yml
# users_manager_group_present_for_group: []

# Must be set in needed host_vars/<your hosts>.yml's files
# users_manager_group_present_for_host: []

#############################################

# Admin group name
# #########################################
# Configuration set for all, group and host are merged
# Precedence: host > group > all > default
# Must be set in group_vars/all.yml
# users_manager_sudo_group_for_all:
#   allow_ssh: false
#   move_home: true
# Must be set in a different group than group_vars/all.yml
# users_manager_sudo_group_for_group: {}

# Must be set in needed host_vars/<your hosts>.yml's files
# users_manager_sudo_group_for_host: {}
users_manager_sudo_group:
  name: users_manager_sudoers
  gid: 65000

# SSH Group allowed to connect on servers
# #########################################
# Configuration set for all, group and host are merged
# Precedence: host > group > all > default
# Must be set in group_vars/all.yml
# users_manager_allow_group_for_all:
#   allow_ssh: false
#   move_home: true
# Must be set in a different group than group_vars/all.yml
# users_manager_allow_group_for_group: {}

# Must be set in needed host_vars/<your hosts>.yml's files
# users_manager_allow_group_for_host: {}
users_manager_allow_group:
  name: users_manager_allow_ssh_access
  gid: 65001

#############################################

# Parameter applied for all users.
# Can be overwritten for specific users in "users_manager_database" variable
# Ref : https://docs.ansible.com/ansible/latest/collections/ansible/builtin/user_module.html
# #########################################
# Configuration set for all, group and host are merged
# Precedence: host > group > all > default
# Must be set in group_vars/all.yml
# users_manager_global_configuration_for_all:
#   allow_ssh: false
#   move_home: true
# Must be set in a different group than group_vars/all.yml
# users_manager_global_configuration_for_group: {}

# Must be set in needed host_vars/<your hosts>.yml's files
# users_manager_global_configuration_for_host: {}
users_manager_global_configuration:
  allow_ssh: true # Allow all users to access to the server with SSH - not a user module parameter
  append: false
  create_home: true
  force: true
  generate_ssh_key: false
  # group: # default: not set
  # groups: # default: not set
  # home: # default: not set
  home_mode: "0750" # set chmod to the home's directory - not a user module parameter
  local: false
  move_home: false
  non_unique: false
  # password_expire_max: # default: not set
  # password_expire_min: # default: not set
  remove: true
  # seuser: # default: not set
  shell: /bin/bash
  # skeleton: # default: not set
  # ssh_key_bits: # default: not set
  # ssh_key_comment: # default: "ansible-generated on $HOSTNAME"
  # ssh_key_passphrase: # default: not set
  ssh_key_type: rsa
  system: false
  umask: "0027"
  update_password: always
  auto_delete_users: false # set to true to manually delete users - not a user module parameter

# Parameter applied for ssh keys.
# #########################################
# Configuration set for all, group and host are merged
# Precedence: host > group > all > default
# Must be set in group_vars/all.yml
# users_manager_allow_group_for_all:
#   allow_ssh: false
#   move_home: true
# Must be set in a different group than group_vars/all.yml
# users_manager_allow_group_for_group: {}

# Must be set in needed host_vars/<your hosts>.yml's files
# users_manager_allow_group_for_host: {}
users_manager_authorized_key:
  # comment: "" # Default: ""
  exclusive: false
  follow: false
  key_options: "" # Default: ""
  manage_dir: true
  path: ~/.ssh/authorized_keys
  validate_certs: true

# Define allowed uid and gid
####################
# Precedence: host > group > all > default
# Must be set in group_vars/all.yml
# users_manager_allow_group_for_all:
#   allow_ssh: false
#   move_home: true
# Must be set in a different group than group_vars/all.yml
# users_manager_allow_group_for_group: {}

# Must be set in needed host_vars/<your hosts>.yml's files
# users_manager_allow_group_for_host: {}
users_manager_uid:
  users:
    minimum: 2000
    maximum: 20000
  groups:
    minimum: 30000
    maximum: 40000

###############################################################
#       Optional configuration with default values            #
###############################################################

# e.g:
# users_manager_groups_database:
#   group_0:
#     gid: 30000
#     system: false
#     local: false

users_manager_groups_database: {}
