# Dyscloud\.Users\_Manager Release Notes

**Topics**

- <a href="#v2-0-1">v2\.0\.1</a>
    - <a href="#release-summary">Release Summary</a>
    - <a href="#minor-changes">Minor Changes</a>
- <a href="#v2-0-0">v2\.0\.0</a>
    - <a href="#release-summary-1">Release Summary</a>
    - <a href="#major-changes">Major Changes</a>
    - <a href="#minor-changes-1">Minor Changes</a>
    - <a href="#breaking-changes--porting-guide">Breaking Changes / Porting Guide</a>

<a id="v2-0-1"></a>
## v2\.0\.1

<a id="release-summary"></a>
### Release Summary

Make configuration more flexible

<a id="minor-changes"></a>
### Minor Changes

* Add comments in defaults/main\.yml
* Create new variables in format \'\_for\_all\'\, \'\_for\_group\'\, \'\_for\_host\' to make the configuration more flexible\. This new behavior is closer to how ansible works with host\_vars and group\_vars\. In addition\, this modification corrects a problem that prevented the modification of a single element of a \'map\' variable\.
* Update molecule tests
* Update variable to apply the same syntax everywhere

<a id="v2-0-0"></a>
## v2\.0\.0

<a id="release-summary-1"></a>
### Release Summary

New feature release

<a id="major-changes"></a>
### Major Changes

* Add GitLab pipeline to test the role
* Add Molecule to test the role
* Add feature to manage SSH group
* Add mode to delete users manually or automatically\. Managed with parameter \"users\_manager\_global\_configuration\[\'auto\_delete\_users\'\]\"
* Add tasks to install requirements

<a id="minor-changes-1"></a>
### Minor Changes

* Add \"static\" configurations as \"dynamic\" configurations that can be modified using variables
* Apply Ansible good practices

<a id="breaking-changes--porting-guide"></a>
### Breaking Changes / Porting Guide

* Variable \"users\_manager\_present\" become a map \(was a list\)
