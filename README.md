# users_manager

[![Maintainer](https://img.shields.io/badge/maintained%20by-dyscloud-e00000?style=flat-square)](https://gitlab.com/dyscloud)
[![License](https://img.shields.io/github/license/dyscloud/users_manager?style=flat-square)](https://gitlab.com/dyscloud/ansible/users_manager/-/blob/master/LICENSE)
[![Release](https://gitlab.com/dyscloud/ansible/users_manager/-/badges/release.svg)](https://gitlab.com/dyscloud/ansible/users_manager/-/releases)
[![pipeline status](https://gitlab.com/dyscloud/ansible/users_manager/badges/master/pipeline.svg)](https://gitlab.com/dyscloud/ansible/users_manager/-/commits/master)[![Ansible version](https://img.shields.io/badge/ansible-%3E%3D2.10-black.svg?style=flat-square&logo=ansible)](https://github.com/ansible/ansible)

⭐ Star us on GitLab — it motivates us a lot!

Create, manage and delete ssh users

**Platforms Supported**:

| Platform | Versions |
|----------|----------|
| Debian | bullseye, bookworm |
| Ubuntu | focal, jammy |

## ⚠️ Requirements

Ansible >= 2.13.

### Ansible role dependencies

None.

## ⚡ Installation

### Install with git

If you do not want a global installation, clone it into your `roles_path`.

```bash
git clone git@gitlab.com:dyscloud/ansible/users_manager.git  users_manager
```

But I often add it as a submodule in a given `playbook_dir` repository.

```bash
git submodule add git@gitlab.com:dyscloud/ansible/users_manager.git roles/users_manager
```

As the role is not managed by Ansible Galaxy, you do not have to specify the
github user account.

### ✏️ Example Playbook

Basic usage is:

```yaml
- hosts: all
  roles:
    - role: users_manager
      vars:
        users_manager_allow_group:
          gid: '65001'
          name: users_manager_allow_ssh_access
        users_manager_authorized_key:
          exclusive: false
          follow: false
          key_options: ''
          manage_dir: true
          path: ~/.ssh/authorized_keys
          validate_certs: true
        users_manager_database: {}
        users_manager_global_configuration:
          allow_ssh: true
          append: false
          auto_delete_users: false
          create_home: true
          force: true
          generate_ssh_key: false
          home_mode: '0750'
          local: false
          move_home: false
          non_unique: false
          remove: true
          shell: /bin/bash
          ssh_key_type: rsa
          system: false
          umask: '0027'
          update_password: always
        users_manager_groups_database: {}
        users_manager_sudo_group:
          gid: '65000'
          name: users_manager_sudoers
        users_manager_uid:
          groups:
            maximum: '40000'
            minimum: '30000'
          users:
            maximum: '20000'
            minimum: '2000'
        
```

## ⚙️ Role Variables

Variables are divided in three types.

The **default vars** section shows you which variables you may
override in your ansible inventory. As a matter of fact, all variables should
be defined there for explicitness, ease of documentation as well as overall
role manageability.

The **context variables** are shown in section below hint you
on how runtime context may affects role execution.

### Default variables
Role default variables from `defaults/main.yml`.

| Variable Name | Value |
|---------------|-------|
| users_manager_database | {}<br> |
| users_manager_sudo_group | gid: '65000'<br>name: users_manager_sudoers<br> |
| users_manager_allow_group | gid: '65001'<br>name: users_manager_allow_ssh_access<br> |
| users_manager_global_configuration | allow_ssh: true<br>append: false<br>auto_delete_users: false<br>create_home: true<br>force: true<br>generate_ssh_key: false<br>home_mode: '0750'<br>local: false<br>move_home: false<br>non_unique: false<br>remove: true<br>shell: /bin/bash<br>ssh_key_type: rsa<br>system: false<br>umask: '0027'<br>update_password: always<br> |
| users_manager_authorized_key | exclusive: false<br>follow: false<br>key_options: ''<br>manage_dir: true<br>path: ~/.ssh/authorized_keys<br>validate_certs: true<br> |
| users_manager_uid | groups:<br>  maximum: '40000'<br>  minimum: '30000'<br>users:<br>  maximum: '20000'<br>  minimum: '2000'<br> |
| users_manager_groups_database | {}<br> |

### Context variables

None.


## Dyscloud\.Users_Manager Release Notes

**Topics**

- <a href="##v2-0-0">v2\.0\.0</a>
  - <a href="##release-summary">Release Summary</a>
  - <a href="##major-changes">Major Changes</a>
  - <a href="##minor-changes">Minor Changes</a>
  - <a href="##breaking-changes--porting-guide">Breaking Changes / Porting Guide</a>

<a id="v2-0-0"></a>

### v2\.0\.0

<a id="release-summary"></a>

#### Release Summary

New feature release

<a id="major-changes"></a>

#### Major Changes

- Add GitLab pipeline to test the role
- Add feature to manage SSH group
- Add mode to delete users manually or automatically\. Managed with parameter \"users_manager_global_configuration\.auto_delete_users\"
- Add tasks to install requirements

<a id="minor-changes"></a>

#### Minor Changes

- Add \"static\" configurations as \"dynamic\" configurations that can be modified using variables
- Apply Ansible good practices

<a id="breaking-changes--porting-guide"></a>

#### Breaking Changes / Porting Guide

- Variable \"users_manager_present\" become a map \(was a list\)


## Author Information

Gregory Lecomte